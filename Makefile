build:
	docker-compose build
up:
	docker-compose up -d
stop:
	docker-compose stop
rm:
	docker-compose rm -f
kill:
	docker-compose kill

logs:
	docker-compose logs

tail:
	docker-compose logs -f 


destroy:
	make stop
	make kill
	make rm

reboot:
	make stop
	make kill
	make up
	docker-compose exec api_platform php composer.phar update

install:
	make build
	make up
	docker-compose exec api_platform php composer.phar update
	make stop
	make up

reinstall:
	make destroy
	make install

#===========================#
#		 	API				#
#===========================#

bash.web:
	docker-compose exec api_platform bash

exec.web:
	docker-compose exec api_platform

tail.web:
	docker-compose logs -f api_platform

logs.web:
	docker-compose logs api_platform

#===========================#
#		 	DB				#
#===========================#

bash.db:
	docker-compose exec database bash

exec.db:
	docker-compose exec database

tail.db:
	docker-compose logs -f database

logs.db:
	docker-compose logs database

create.db:
	docker-compose exec api_platform php bin/console doctrine:database:create

update.db:
	docker-compose exec api_platform php bin/console doctrine:schema:update --force