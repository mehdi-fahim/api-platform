FROM php:7.2-apache-stretch

RUN apt update

RUN apt install -y \
    git \
    curl \
    gcc \
    build-essential \
    zlib1g-dev \
    libicu-dev \
    g++ \
    libfreetype6-dev \
    libjpeg62-turbo-dev \
    libmcrypt-dev \
    libpng-dev \
    libxml2-dev \
    libgmp-dev \
    libssl-dev \
    libcurl4-openssl-dev \
    pkg-config

RUN pecl install \
    xdebug-2.6.0

RUN docker-php-ext-enable \
    xdebug

RUN docker-php-ext-install -j$(nproc) \
    intl \
    gd \
    pdo \
    curl \
    json \
    gmp \
    mbstring \
    xml \
    zip

RUN docker-php-ext-configure \
    pdo_mysql

RUN docker-php-ext-install -j$(nproc) \
    pdo_mysql

RUN a2enmod headers \
    rewrite \
    ssl